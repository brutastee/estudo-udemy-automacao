import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class TesteFrameEJanelasPopUp {

    private WebDriver driver;

    @Before
    public void iniciarlizarNavegador() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Tialison\\Desktop\\drivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("file:///" + System.getProperty("user.dir") + "/src/main/resources/componentes.html");
    }

    @After
    public void fecharNavegador() {
        driver.quit();
    }

    @Test
    public void devoInteragirComFrame() {
        driver.switchTo().frame("frame1");

        driver.findElement(By.id("frameButton")).click();
        Alert alert = driver.switchTo().alert();

        String textoFrame = alert.getText();

        Assert.assertEquals("Frame OK!", textoFrame);
        alert.accept();

        driver.switchTo().defaultContent();

        driver.findElement(By.id("elementosForm:nome")).sendKeys(textoFrame);
    }

    @Test
    public void devoInteragirComJanelaPopUp() {
        driver.findElement(By.id("buttonPopUpEasy")).click();

        driver.switchTo().window("Popup");
        driver.findElement(By.tagName("textarea")).sendKeys("ula la");
        driver.close();

        driver.switchTo().window("");
        driver.findElement(By.tagName("textarea")).sendKeys("ula la baby");
    }


    @Test
    public void devoInteragirComJanelaPopUpSemTitulo() {
        driver.findElement(By.id("buttonPopUpHard")).click();


        driver.switchTo().window((String) driver.getWindowHandles().toArray()[1]);
        driver.findElement(By.tagName("textarea")).sendKeys("VAI MANO");
        driver.close();

        driver.switchTo().window((String) driver.getWindowHandles().toArray()[0]);
        driver.findElement(By.tagName("textarea")).sendKeys("VAI tio");

    }

}
