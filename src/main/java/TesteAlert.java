import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class TesteAlert {

    private WebDriver driver;

    @Before
    public void iniciarlizarNavegador() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Tialison\\Desktop\\drivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("file:///" + System.getProperty("user.dir") + "/src/main/resources/componentes.html");
    }

    @After
    public void fecharNavegador() {
        driver.quit();
    }

    @Test
    public void deveInteragirComAlertSimples() {
        driver.findElement(By.id("alert")).click();

        Alert alert = driver.switchTo().alert();
        String textoAlert = alert.getText();

        Assert.assertEquals("Alert Simples", textoAlert);
        alert.accept();

        driver.findElement(By.id("elementosForm:nome")).sendKeys(textoAlert);
    }


    @Test
    public void deveInteragirComConfirmeSimples() {
        driver.findElement(By.id("confirm")).click();
        Alert alert = driver.switchTo().alert();
        alert.accept();
        String textoConfirme = alert.getText();
        Assert.assertEquals("Confirmado", textoConfirme);
        alert.accept();

        driver.findElement(By.id("confirm")).click();
        driver.switchTo().alert();
        alert.dismiss();
        String textoNegado = alert.getText();
        Assert.assertEquals("Negado", textoNegado);
        alert.dismiss();
    }

    @Test
    public void deveInteragirComPrompt() {
        driver.findElement(By.id("prompt")).click();
        Alert alert = driver.switchTo().alert();
        Assert.assertEquals("Digite um numero", alert.getText());
        alert.sendKeys("14");
        alert.accept();
        Assert.assertEquals("Era 14?", alert.getText());
        alert.accept();
        Assert.assertEquals(":D", alert.getText());
        alert.accept();

        driver.findElement(By.id("prompt")).click();
        driver.switchTo().alert();
        alert.dismiss();
        Assert.assertEquals("Era null?", alert.getText());
        alert.dismiss();
        Assert.assertEquals(":(", alert.getText());
        alert.accept();

    }
}
