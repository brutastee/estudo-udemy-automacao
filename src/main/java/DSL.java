import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;
import java.util.List;

public class DSL {

    private WebDriver driver;

    public DSL(WebDriver driver) {
        this.driver = driver;
    }

    /************* TextField & TextArea ***********/

    public void escrever(By by, String texto) {
        driver.findElement(by).clear();
        driver.findElement(by).sendKeys(texto);
    }

    public void escrever(String element, String texto) {
        escrever(By.id(element), texto);
    }

    public String obterAttributoValue(String element) {
        return driver.findElement(By.id(element)).getAttribute("value");
    }

    /************* Radio & Check ****************/

    public void clicarRadio(String element) {
        driver.findElement(By.id(element)).click();
    }

    public boolean verificarSeRadioEstaMarcado(String element) {
        return driver.findElement(By.id(element)).isSelected();
    }

    public void clicarCheck(String element) {
        driver.findElement(By.id(element)).click();
    }

    public boolean verificarSeCheckEstaMarcado(String element) {
        return driver.findElement(By.id(element)).isSelected();
    }

    /***************** Combo *******************/

    public void selecionarCombo(String element, String valor) {
        WebElement elementCombo = driver.findElement(By.id(element));
        Select combo = new Select(elementCombo);
        combo.selectByVisibleText(valor);
    }

    public void deselecionarCombo(String element, String valor) {
        WebElement elementCombo = driver.findElement(By.id(element));
        Select select = new Select(elementCombo);
        select.deselectByVisibleText(valor);
    }

    public String obterValorCombo(String element) {
        WebElement elementCombo = driver.findElement(By.id(element));
        Select combo = new Select(elementCombo);
        return combo.getFirstSelectedOption().getText();
    }

    public List<String> obterValoresCombo(String element) {
        WebElement elementCombo = driver.findElement(By.id(element));
        Select combo = new Select(elementCombo);
        List<WebElement> allSelectedOption = combo.getAllSelectedOptions();
        List<String> valores = new ArrayList<>();
        for (WebElement opcao : allSelectedOption) {
            valores.add(opcao.getText());
        }
        return valores;
    }

    public int obterQuantidadeDeOpcoesNoCombo(String element) {
        WebElement elementCombo = driver.findElement(By.id(element));
        Select combo = new Select(elementCombo);
        List<WebElement> valores = combo.getOptions();
        return valores.size();
    }

    public boolean verificarSeUmaOpcaoEstaPresenteNoCombo(String element, String opcao) {
        WebElement elementCombo = driver.findElement(By.id(element));
        Select combo = new Select(elementCombo);
        List<WebElement> valores = combo.getOptions();
        for (WebElement option : valores) {
            if (option.getText().trim().equals(opcao)) {
                return true;
            }
        }
        return false;
    }

    /*************** Botão ******************/

    public void clicar(String element) {
        driver.findElement(By.id(element)).click();
    }

    /**************** Link ******************/

    public void clicarLink(String element) {
        driver.findElement(By.linkText(element)).click();
    }

    /************** Texto ********************/

    public String obterTexto(By by) {
        return driver.findElement(by).getText().trim();
    }

    public String obterTexto(String element) {
        return obterTexto(By.id(element));
    }

    /***************** Alerts ************************/

    public String alertObterTexto(String element) {
        Alert alert = driver.switchTo().alert();
        return alert.getText().trim();
    }

    public String alertObterTextoEAceitar(String element) {
        Alert alert = driver.switchTo().alert();
        String valor = alert.getText().trim();
        alert.accept();
        return valor;
    }

    public String alertObterTextoENegar(String element) {
        Alert alert = driver.switchTo().alert();
        String valor = alert.getText().trim();
        alert.dismiss();
        return valor;
    }


    public void alertEscrever(String texto) {
        Alert alert = driver.switchTo().alert();
        alert.sendKeys(texto);
        alert.accept();
    }

    /*************** Janelas & Frames *********************/

    public void entrarFrame(String element) {
        driver.switchTo().frame(element);
    }

    public void sairFrame(String element) {
        driver.switchTo().defaultContent();
    }

    public void trocarJanela(String element) {
        driver.switchTo().window(element);
    }
}
