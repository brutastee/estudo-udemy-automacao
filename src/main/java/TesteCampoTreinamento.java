import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.Arrays;
import java.util.List;

public class TesteCampoTreinamento {
    private WebDriver driver;
    DSL dsl;

    @Before
    public void iniciarlizarNavegador() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Tialison\\Desktop\\drivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("file:///" + System.getProperty("user.dir") + "/src/main/resources/componentes.html");
        dsl = new DSL(driver);
    }

    @After
    public void fecharNavegador() {
        driver.quit();
    }

    @Test
    public void testeTextField() {
        dsl.escrever("elementosForm:nome", "Teste");

        Assert.assertEquals("Teste", dsl.obterAttributoValue("elementosForm:nome"));
    }

    @Test
    public void testePreencherCampoLimparEPreencherNovamete() {
        dsl.escrever("elementosForm:nome", "Bruno");
        Assert.assertEquals("Bruno", dsl.obterAttributoValue("elementosForm:nome"));

        dsl.escrever("elementosForm:nome", "Souza");
        Assert.assertEquals("Souza", dsl.obterAttributoValue("elementosForm:nome"));
    }

    @Test
    public void deveInteragirComTextArea() {
        dsl.escrever("elementosForm:sugestoes", "Teste");

        Assert.assertEquals("Teste", dsl.obterAttributoValue("elementosForm:sugestoes"));
    }

    @Test
    public void deveInteragirComRadioButtom() {
        dsl.clicarRadio("elementosForm:sexo:0");
        Assert.assertTrue(dsl.verificarSeRadioEstaMarcado("elementosForm:sexo:0"));

        dsl.clicarRadio("elementosForm:comidaFavorita:0");
        Assert.assertTrue(dsl.verificarSeRadioEstaMarcado("elementosForm:comidaFavorita:0"));
    }

    @Test
    public void deveInteragirComCombo() {
        dsl.selecionarCombo("elementosForm:escolaridade", "Superior");
        Assert.assertEquals("Superior", dsl.obterValorCombo("elementosForm:escolaridade"));

    }

    @Test
    public void deveVerificarOpcoesCombo() {
        Assert.assertEquals(8, dsl.obterQuantidadeDeOpcoesNoCombo("elementosForm:escolaridade"));
        Assert.assertTrue(dsl.verificarSeUmaOpcaoEstaPresenteNoCombo("elementosForm:escolaridade", "Superior"));
    }

    @Test
    public void deveVerificarComboComMultiplasOpcoes() {
        dsl.selecionarCombo("elementosForm:esportes", "Natacao");
        dsl.selecionarCombo("elementosForm:esportes", "Corrida");
        dsl.selecionarCombo("elementosForm:esportes", "Karate");
        List<String> opcoesMarcadas = dsl.obterValoresCombo("elementosForm:esportes");
        Assert.assertEquals(3, opcoesMarcadas.size());

        dsl.deselecionarCombo("elementosForm:esportes", "Corrida");
        opcoesMarcadas = dsl.obterValoresCombo("elementosForm:esportes");
        Assert.assertEquals(2, opcoesMarcadas.size());
        Assert.assertTrue(opcoesMarcadas.containsAll(Arrays.asList("Natacao", "Karate")));
    }

    @Test
    public void deveInteragirComBotoes() {
        dsl.clicar("buttonSimple");
        Assert.assertEquals("Obrigado!", dsl.obterAttributoValue("buttonSimple"));
    }

    @Test
    @Ignore
    public void deveInteragirComLinks() {
        dsl.clicarLink("Voltar");

        Assert.assertEquals("Voltou!", dsl.obterTexto("resultado"));
    }

    @Test
    public void deveInteragirComTextos() {
//        Assert.assertTrue(driver.findElement(By.tagName("body"))
//                .getText().contains("Campo de Treinamento"));

        Assert.assertEquals("Campo de Treinamento", dsl.obterTexto(By.tagName("h3")));
        Assert.assertEquals("Cuidado onde clica, muitas armadilhas...", dsl.obterTexto(By.className("facilAchar")));

    }
}