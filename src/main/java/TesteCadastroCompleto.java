import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;

public class TesteCadastroCompleto {

    private WebDriver driver;
    private DSL dsl;

    @Before
    public void iniciarlizarNavegador() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Tialison\\Desktop\\drivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("file:///" + System.getProperty("user.dir") + "/src/main/resources/componentes.html");
        dsl = new DSL(driver);
    }

    @After
    public void fecharNavegador() {
        driver.quit();
    }

    @Test
    public void cadastroCompleto() {
        dsl.escrever("elementosForm:nome", "Bruno");
        dsl.escrever("elementosForm:sobrenome", "Souza");

        dsl.clicarRadio("elementosForm:sexo:0");
        dsl.clicarRadio("elementosForm:comidaFavorita:2");

        dsl.selecionarCombo("elementosForm:escolaridade", "Superior");
        dsl.selecionarCombo("elementosForm:esportes", "Natacao");

        dsl.clicar("elementosForm:cadastrar");

        List<WebElement> textos = driver.findElements(By.xpath("(//div)[2]//span"));

        for (WebElement elementos : textos) {
            System.out.println(elementos.getText());
        }

        Assert.assertEquals("Bruno", dsl.obterTexto(By.xpath("(//div[2]//span)[2]")));
        Assert.assertEquals("Souza", dsl.obterTexto(By.xpath("(//div[2]//span)[3]")));
        Assert.assertEquals("Masculino", dsl.obterTexto(By.xpath("(//div[2]//span)[4]")));
        Assert.assertEquals("Pizza", dsl.obterTexto(By.xpath("(//div[2]//span)[5]")));
        Assert.assertEquals("superior", dsl.obterTexto(By.xpath("(//div[2]//span)[6]")));
        Assert.assertEquals("Natacao", dsl.obterTexto(By.xpath("(//div[2]//span)[7]")));

    }

}
